const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.json());

app.post('/summary', (req, res) => {
  
  try {
    const { orders, isMember } = req.body;
    if (!orders) {
      throw {status: 400, error: 'Missing data field'};
    } else {
      const price = {
        'red': 50,
        'green': 40,
        'blue': 30,
        'yellow': 50,
        'pink': 80,
        'purple': 90,
        'orange': 120
      }
  
      const setPromotionDiscount = ['orange', 'pink', 'green']
      const promotionMulti = 2 
  
      let summary = 0


      for (const key in orders) {
        if (price[key]) {
          console.log('setPromotionDiscount :: ', typeof setPromotionDiscount)
          console.log('key :: ', typeof key)
          
          if (+orders[key] >= promotionMulti && setPromotionDiscount.includes(key)) {
            numDis = Math.floor(orders[key] / promotionMulti) * promotionMulti
            remain = orders[key] - numDis
            summary += ((numDis * 0.95) + remain) * price[key]
          } else {
            summary += price[key] * orders[key]
          }
        } else {
          throw {status: 400, error: `Order don't have in menu`};
        }
      }

      if (isMember) {
        summary = summary * 0.9
      }
      
      console.log('summary :: ', summary)
      res.status(200).json({ status: '200', desc: 'Success', 'summary': summary });

    }
    
  } catch(e) {
    return res.status(e.status || 400).json({ status: '400', desc: e.error || e.message});

  }
  
});

app.listen(8080, () => {
  console.log('Server is listening on port 8080');
});

module.exports = app;
