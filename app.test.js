const request = require('supertest');
const app = require('./app');

describe('POST /summary', () => {
  it('should calculate the summary of an array', async () => {
    const dataTest = {
        isMember: true,
        orders: {
            "orange": 3,
            "pink": 2,
            "purple": 4
        }
    };
    const response = await request(app).post('/summary').send(dataTest);
    console.log('response.body :: ', response.body)
    expect(response.status).toBe(200);
    expect(response.body.summary).toEqual(774);
  });

  it('should calculate the summary of an array not a member', async () => {
    const dataTest = {
        isMember: false,
        orders: {
            "orange": 3,
            "pink": 2,
            "purple": 4
        }
    };
    const response = await request(app).post('/summary').send(dataTest);
    console.log('response.body :: ', response.body)
    expect(response.status).toBe(200);
    expect(response.body.summary).toEqual(860);
  });


  it('should orders do not have in menu', async () => {
    const dataTest = {
        isMember: true,
        orders: {
            "orangesss": 3,
            "pink": 2,
            "purple": 4
        }
    };
    const response = await request(app).post('/summary').send(dataTest);
    expect(response.status).toBe(400);
    expect(response.body.desc).toEqual(`Order don't have in menu`);
  });

  it('should return an error if data field is missing', async () => {
    const response = await request(app).post('/summary');
    expect(response.status).toBe(400);
    expect(response.body.desc).toEqual('Missing data field');
  });
});